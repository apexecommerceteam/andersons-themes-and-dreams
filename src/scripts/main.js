document.addEventListener("DOMContentLoaded", function(event) {
  $('.header-usp').slick({
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 4,
    autoplay: true,
    arrows: true,
    prevArrow: '<div class="slick-prev"><i class="fal fa-angle-left"></i></div>',
    nextArrow: '<div class="slick-next"><i class="fal fa-angle-right"></i></div>',
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          infinite: true,
          arrows: true,
          prevArrow: '<div class="slick-prev"><i class="fal fa-angle-left"></i></div>',
          nextArrow: '<div class="slick-next"><i class="fal fa-angle-right"></i></div>',
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          arrows: true,
          prevArrow: '<div class="slick-prev"><i class="fal fa-angle-left"></i></div>',
          nextArrow: '<div class="slick-next"><i class="fal fa-angle-right"></i></div>',
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          prevArrow: '<div class="slick-prev"><i class="fal fa-angle-left"></i></div>',
          nextArrow: '<div class="slick-next"><i class="fal fa-angle-right"></i></div>',
        }
      }
    ]
  });

  if ($('#testimonials').length > 0) {
    $('#testimonials').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
      autoplay: true,
			adaptiveHeight: true
    });
  }

  window.addEventListener('scroll', function() {
    console.log('scrolling');
    $('.jas-masonry').isotope('layout');
  });



});
