var AgreeToTerms = {
    checkboxClass : '.js-agreeTerms',
    checkoutBtnClass : '.js-checkout-btn',
    toggleCheckoutBtn : function() {
        // toggle diabled property
        $(AgreeToTerms.checkoutBtnClass).prop('disabled', function(i, v) { return !v; });
        $(AgreeToTerms.checkoutBtnClass).toggleClass('disabled');
    }
}

document.addEventListener("DOMContentLoaded", function(event) {
    $(AgreeToTerms.checkboxClass).click(function(){
        // listen to checkbox check
        AgreeToTerms.toggleCheckoutBtn();
    });

    $(AgreeToTerms.checkoutBtnClass).click(function(e){
        if ($(this).hasClass('disabled')) {
            e.preventDefault();
            console.log('clicked');
        }
    });
});
