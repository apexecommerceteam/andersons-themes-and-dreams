const gulp = require("gulp");
const sass = require("gulp-sass");
sass.compiler = require("node-sass");
const watch = require("gulp-watch");

const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');

const jsFiles = 'src/scripts/**/*.js',
    jsDest = 'assets';

const sassFiles = "src/scss/main.scss",
    sassDest = "assets";

gulp.task("sass", function() {
  return gulp
    .src(sassFiles)
    .pipe(sass().on("error", sass.logError))
    .pipe(rename('main.css.liquid'))
    .pipe(gulp.dest(sassDest));
});

gulp.task('scripts', function() {
  return gulp.src(jsFiles)
      .pipe(concat('theme.js'))
      .pipe(gulp.dest(jsDest))
      .pipe(sourcemaps.write())
      .pipe(rename('theme.min.js'))
      .pipe(uglify().on('error', function (e) {
        console.log(e.message);
        this.emit('end');
      }))
      .pipe(gulp.dest(jsDest));
});

gulp.task("watch", function() {
  gulp.watch("src/scss/**/*.scss", ["sass"]);
  gulp.watch("src/scripts/*.js", ["scripts"]);
});

gulp.task("default", ["sass", "scripts"]);
